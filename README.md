# MTCaptcha2

Fork of the PeerTube plugin [MTCaptcha](https://www.mtcaptcha.com/) by [Rigelk](https://framagit.org/rigelk), adding a mini captcha widget to the the signup.

This fork fixes some display issues:

- Keep displaying the widget when the user navigates with the router app
- Keep displaying the widget on all signup steps
- Use more neutral colors (PeerTube grey)

## Screenshots

![](https://framagit.org/kimsible/peertube-plugin-mtcaptcha2/-/raw/main/screenshots/signup.png)

