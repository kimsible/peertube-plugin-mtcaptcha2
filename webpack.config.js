const path = require("path")

let config = {
  entry: "./client/signup-client-plugin.js",
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "./signup-client-plugin.js",
    library: {
      type: "module"
    }
  },
  experiments: {
    outputModule: true
  }
}

module.exports = config
