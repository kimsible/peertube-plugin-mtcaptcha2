import loadjs from 'loadjs'

function register({ registerHook, peertubeHelpers }) {
  initMTC(registerHook, peertubeHelpers).catch(err =>
    console.error('Cannot initialize mtcaptcha plugin', err)
  )
}

export { register }

function initMTC(registerHook, peertubeHelpers) {
  return peertubeHelpers.getSettings().then(async s => {
    if (!s || !s['mtcaptcha-site-key']) {
      console.error(
        'MTCaptcha plugin lacks a site key set to use the MTCaptcha. Falling back to normal registration.'
      )
      return
    }

    window.mtcaptchaConfig = {
      'sitekey': s['mtcaptcha-site-key'],
      'render': 'explicit',
      'widgetSize': 'mini',
      'miniFormWidth': 265,
      'miniFormHeight': 42,
      'customStyle': {
        'cardColor': '#FAFAFA',
        'cardBorder': '#C6C6C6',
        'inputBackgroundColor': '#FAFAFA',
        'inputBorderColor': {
          'byDefault': '#C6C6C6',
          'hover': '#C6C6C6',
          'active': '#C6C6C6'
        }
       },
      'verified-callback': state => window.MTCaptchaLoadCallbackResponse = state.verifiedToken,
      'renderQueue': []
    }

    loadjs([
      'https://service.mtcaptcha.com/mtcv1/client/mtcaptcha.min.js',
      'https://service2.mtcaptcha.com/mtcv1/client/mtcaptcha2.min.js',
    ])

    // add captcha to the last step of the register stepper on router navigation
    const MTCaptchaID = await insertMTCaptchaContainer('my-custom-stepper .container')
    window.mtcaptchaConfig.renderQueue.push(MTCaptchaID)

    registerHook({
      target: 'action:router.navigation-end',
      handler: async ({ path }) => {
        if (path === '/signup') {
          // add captcha to the last step of the register stepper on router navigation
          const MTCaptchaID = await insertMTCaptchaContainer('my-custom-stepper .container')
          window.mtcaptchaConfig.renderQueue.push(MTCaptchaID)
        }
      }
    })

    registerHook({
      target: 'filter:api.signup.registration.create.params',
      handler: body =>
        Object.assign({}, body, {
          'mtcaptcha-response': window.MTCaptchaLoadCallbackResponse
        })
    })
  })
}

async function insertMTCaptchaContainer (stepperSelector) {
  await rendering(stepperSelector)
  const stepper = document.querySelector(stepperSelector)
  const div = document.createElement('div')
  div.id = `mtcaptcha-${Math.random().toString(16).slice(2)}`
  div.classList.add('mtcaptcha')
  div.style = 'width:270px;height:85px;display:flex;align-items:center;margin:auto'
  stepper.appendChild(div)
  return div.id
}

async function rendering (selector) {
  // Waiting for DOMContent updated with a timeout of 5 seconds
  await new Promise((resolve, reject) => {
    const timeout = setTimeout(() => {
      clearInterval(interval)
      reject(new Error('DOMContent cannot be loaded'))
    }, 5000)

    // Waiting for videofile input in DOM
    const interval = setInterval(() => {
      if (document.querySelector(selector) !== null) {
        clearTimeout(timeout)
        clearInterval(interval)
        resolve()
      }
    }, 10)
  })
}
